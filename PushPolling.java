package dmoda.mobile;

import java.io.IOException;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;
import java.util.SimpleTimeZone;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.sql.DriverManager;
import java.util.Calendar;
import com.sybase.jdbcx.SybDriver;
import java.sql.Date;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Message.Builder;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

/*
 QUERY DI PROVA
 SELECT tbsgstag as result from libemme.agtbsg where libemme.agtbsg.x_upid=1444 (d� 0 righe, ma la struttura � corretta per raw_query)
 */

public class PushPolling {
	// got it from Google API account
	private static String apiKeyServer = "AIzaSyD6jnbKJSpedTTH2Zvf2uDxBoy6fpc30L8";
	private static String apnCertificate = "conf/apn_certificate.p12";
	private static String apnPassword = "comino";
	// time at which "end of day" messages have to be delivered (for 20:30
	// endOfDayHours=20, endOfDayMinutes=30)
	private static int endOfDayHours = 20;
	private static int endOfDayMinutes = 00;
	// seconds of sleeping for the thread
	private static int secondsToWait = 1;
	// message sent in push notification TODO change it
	static String textMessage = "ok";

	private static int messageContentMaxSize = 2000;

	public static void main(String args[]) {
		System.out.println("Push Polling Daemon");
		MySqlDb mysqlDb = new MySqlDb("192.168.1.20", "dmoda_mobile",
				"dmoda_mobilepw", "dmoda_mobile");
		Statement mysqlEventClassStatement = mysqlDb.connect();
		Statement mysqlStoreListStatement = mysqlDb.connect();


		while (true) {
			try {
				if (mysqlEventClassStatement.isClosed()) {
					mysqlEventClassStatement=mysqlDb.connect();
				}
				if (mysqlStoreListStatement.isClosed()) {
					mysqlEventClassStatement=mysqlDb.connect();
				}
			} catch (SQLException e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
			}
			ResultSet mysqlDbEventClassResults = null;
			ResultSet otherResults;

			try {
				mysqlDbEventClassResults = mysqlEventClassStatement
						.executeQuery("SELECT * from push_to_send");
			} catch (SQLException e2) {
				try {
					if (mysqlEventClassStatement.isClosed()) {
						mysqlEventClassStatement=mysqlDb.connect();
					}
					if (mysqlStoreListStatement.isClosed()) {
						mysqlEventClassStatement=mysqlDb.connect();
					}
				} catch (SQLException e3) {
					// TODO Auto-generated catch block
					e3.printStackTrace();
				}
				e2.printStackTrace();
			}

			try {
				while (mysqlDbEventClassResults.next()) {
					int id = mysqlDbEventClassResults.getInt("push_id");
					String pushToken = mysqlDbEventClassResults
							.getString("push_token");
					int messageSpoolId = mysqlDbEventClassResults.getInt("message_spool_id");

					//ANDROID
					PushService pushService = new PushService(apiKeyServer,	"android", pushToken, textMessage, 5, apnCertificate,apnPassword);
					
					//iOS
					//PushService pushService = new PushService(apiKeyServer,"ios", pushToken, textMessage, 5, apnCertificate,apnPassword);
					
					pushService.call();
					
					GregorianCalendar datetimeCorrente=new GregorianCalendar();
					System.out.println("Inviato: "+datetimeCorrente.getTime());

					try {
						mysqlStoreListStatement
								.executeUpdate("DELETE FROM push_to_send WHERE push_id="
										+ id);
						
						mysqlStoreListStatement.executeUpdate("UPDATE message_spool SET message_spool_push_sent=true WHERE message_spool_id="+messageSpoolId);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			} catch (SQLException e1) {
				try {
					if (mysqlEventClassStatement.isClosed()) {
						mysqlEventClassStatement=mysqlDb.connect();
					}
					if (mysqlStoreListStatement.isClosed()) {
						mysqlEventClassStatement=mysqlDb.connect();
					}
				} catch (SQLException e3) {
					// TODO Auto-generated catch block
					e3.printStackTrace();
				}
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}


			
			
			try {
				Thread.sleep(secondsToWait * 1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}
	}
}
