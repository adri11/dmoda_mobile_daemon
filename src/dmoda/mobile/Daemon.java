package dmoda.mobile;

import java.io.IOException;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;
import java.util.SimpleTimeZone;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.sql.DriverManager;
import java.util.Calendar;
import com.sybase.jdbcx.SybDriver;
import java.sql.Date;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Message.Builder;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

/*
 QUERY DI PROVA
 SELECT tbsgstag as result from libemme.agtbsg where libemme.agtbsg.x_upid=1444 (d� 0 righe, ma la struttura � corretta per raw_query)
 */

public class Daemon {
	// got it from Google API account
	private static String apiKeyServer = "AIzaSyD6jnbKJSpedTTH2Zvf2uDxBoy6fpc30L8";
	private static String apnCertificate = "conf/apn_certificate.p12";
	private static String apnPassword = "comino";
	
	// time at which "end of day" messages have to be delivered (for 20:30 endOfDayHours=20, endOfDayMinutes=30)
	private static int endOfDayHours = 20;
	private static int endOfDayMinutes = 00;
	//seconds of sleeping for the thread
	private static int secondsToWait=60;
	//message sent in push notification TODO change it
	static String textMessage="ok";
	
	
	private static int messageContentMaxSize=2000;

	public static void main(String args[]) {
		System.out.println("D Moda Mobile Daemon");
		MySqlDb mysqlDb = new MySqlDb("localhost", "dmoda_mobile", "dmoda_mobilepw", "dmoda_mobile");
		LegacyDb legacyDb = new LegacyDb("192.168.83.3", "2638", "DBA", "SQL","LIBBALDI");

		ResultSet mysqlDbEventClassResults;
		ResultSet legacyDbResults;

		Statement mysqlEventClassStatement = mysqlDb.connect();
		Statement mysqlStoreListStatement = mysqlDb.connect();
		Statement tempStatement=mysqlDb.connect();
		Statement legacyStatement = legacyDb.connect();
		int j=0;


		while (true) {
			j++;
			
			try {
				if (mysqlEventClassStatement.isClosed()) {
					mysqlEventClassStatement=mysqlDb.connect();					
				}
				if (mysqlStoreListStatement.isClosed()) {
					mysqlStoreListStatement=mysqlDb.connect();					
				}
				if (tempStatement.isClosed()) {
					tempStatement=mysqlDb.connect();					
				}
				if (legacyStatement.isClosed()) {
					legacyStatement = legacyDb.connect();				
				}
			} catch (SQLException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}

			Connection mysqlDbConnection = mysqlDb.getConnection();
			
			//to don't make connection dieing
			if (j==5) {
				j=0;
				try {
					legacyStatement.executeQuery("SELECT 1");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			PreparedStatement mysqlPrepStatement = null;
			String query = null;

			try {
				// Get the events that need to be checked on legacyDb
				mysqlDbEventClassResults = mysqlEventClassStatement.executeQuery("SELECT * " + "FROM event_class " + "WHERE event_class_deleted = FALSE " + "AND event_class_active = TRUE "
				// period not expired, testata 100%
						+ "AND (event_class_period_date_start IS NULL OR (event_class_period_date_start <= CURDATE() AND event_class_period_date_end >=CURDATE())) "
						// that it's the time to check, perfetta
						+ "AND (event_class_last_check IS NULL OR DATE_ADD(event_class_last_check, INTERVAL event_class_check_interval MINUTE) < NOW()) "
						// testato
						+ "AND NOT (event_class_target_reached = TRUE AND event_class_period_type = 'dates') "
						// testato
						+ "AND NOT (event_class_target_reached = TRUE AND event_class_period_type = 'days' AND DATE_ADD(event_class_last_period_current_start, INTERVAL event_class_period_days-1 DAY) >= CURDATE()) ");
				/* a questo punto si � sicuri che � attivo, non eliminato, */
				/*
				 * non scaduto, che � ora di verificarlo e che non � gi� scaturito nel periodo prescelto
				 */


				
				GregorianCalendar currentDateTime = new GregorianCalendar();
				//TODO Togliere dopo la simulazione
				//currentDateTime.add(GregorianCalendar.YEAR, -2);
				GregorianCalendar currentDate=new GregorianCalendar(currentDateTime.get(GregorianCalendar.YEAR), currentDateTime.get(GregorianCalendar.MONTH), currentDateTime.get(GregorianCalendar.DAY_OF_MONTH));
				
				
				GregorianCalendar endOfDay=(GregorianCalendar) currentDate.clone();
				endOfDay.set(GregorianCalendar.HOUR_OF_DAY, endOfDayHours);
				endOfDay.set(GregorianCalendar.MINUTE, endOfDayMinutes);
				
				Timestamp currentSqlDateTime = new Timestamp(currentDateTime.getTimeInMillis());
				Timestamp currentSqlDate=new Timestamp(currentDate.getTimeInMillis());
				
				System.out.println("Eventi che vengono verificati su legacy db:");

				while (mysqlDbEventClassResults.next()) {

					Boolean eventHappened = false;
					Boolean eventChecked = false;

					// Retrieve values of ALL the columns
					int eventClassId = mysqlDbEventClassResults.getInt("event_class_id");
					String eventClassName = mysqlDbEventClassResults.getString("event_class_name");
					String eventClassDescription = mysqlDbEventClassResults.getString("event_class_description");
					String eventClassType = mysqlDbEventClassResults.getString("event_class_type");
					int eventClassCheckInterval = mysqlDbEventClassResults.getInt("event_class_check_interval");
					int eventClassIntReferenceValue = mysqlDbEventClassResults.getInt("event_class_int_reference_value");
					String eventClassPeriodType = mysqlDbEventClassResults.getString("event_class_period_type");
					Date eventClassPeriodDateStart = mysqlDbEventClassResults.getDate("event_class_period_date_start");
					Date eventClassPeriodDateEnd = mysqlDbEventClassResults.getDate("event_class_period_date_end");
					int eventClassPeriodDays = mysqlDbEventClassResults.getInt("event_class_period_days");
					String eventClassStructuredComparisionType = mysqlDbEventClassResults.getString("event_class_structured_comparision_type");
					String eventClassStructuredValueType = mysqlDbEventClassResults.getString("event_class_structured_value_type");
					boolean eventClassStructuredAllStores = mysqlDbEventClassResults.getBoolean("event_class_structured_all_stores");
					String eventClassQuery = mysqlDbEventClassResults.getString("event_class_query");
					String eventClassComparedQueryComparisionType = mysqlDbEventClassResults.getString("event_class_compared_query_comparision_type");
					String eventClassComparedQueryStringReferenceValue = mysqlDbEventClassResults.getString("event_class_compared_query_string_reference_value");
					int eventClassDeltaCounter = mysqlDbEventClassResults.getInt("event_class_delta_counter");
					boolean eventClassTargetReached = mysqlDbEventClassResults.getBoolean("event_class_target_reached");
					Date eventClassLastPeriodCurrentStart = mysqlDbEventClassResults.getDate("event_class_last_period_current_start");
					Timestamp eventClassLastCheck = mysqlDbEventClassResults.getTimestamp("event_class_last_check");
					String eventClassLastQueryResult = mysqlDbEventClassResults.getString("event_class_last_query_result");
					boolean eventClassActive = mysqlDbEventClassResults.getBoolean("event_class_active");
					boolean eventClassDeleted = mysqlDbEventClassResults.getBoolean("event_class_deleted");
					
					System.out.println("id= " + eventClassId + " nome= " + eventClassName);
					GregorianCalendar subPeriodStartReference=new GregorianCalendar();
					GregorianCalendar subPeriodEndReference=new GregorianCalendar();
					//periodType=days
					if (eventClassPeriodType.equals("days")) {
						GregorianCalendar testValue = new GregorianCalendar();
						testValue.setTimeInMillis(eventClassLastPeriodCurrentStart.getTime());
						testValue.add(GregorianCalendar.DAY_OF_MONTH, eventClassPeriodDays - 1);
						//if the sub period is expired, i set last_period_current_start to proper value
						if (currentDate.after(testValue)) {
							eventClassLastPeriodCurrentStart.setTime(currentDate.getTimeInMillis());
							eventClassTargetReached = false;
							eventClassDeltaCounter = 0;
							query = "UPDATE event_class SET event_class_last_period_current_start=?, event_class_target_reached=FALSE, event_class_delta_counter=0 WHERE event_class_id=?";
							mysqlPrepStatement = mysqlDbConnection.prepareStatement(query);
							mysqlPrepStatement.setDate(1, eventClassLastPeriodCurrentStart);
							mysqlPrepStatement.setInt(2, eventClassId);
							mysqlPrepStatement.executeUpdate();
						}
						subPeriodStartReference.setTimeInMillis(eventClassLastPeriodCurrentStart.getTime());
						subPeriodEndReference=(GregorianCalendar) subPeriodStartReference.clone();
						subPeriodEndReference.add(GregorianCalendar.DAY_OF_MONTH, eventClassPeriodDays-1);	

					}
					//periodType=dates
					else {
						subPeriodStartReference.setTimeInMillis(eventClassPeriodDateStart.getTime());
						subPeriodEndReference.setTimeInMillis(eventClassPeriodDateEnd.getTime());
					}
					
					SimpleDateFormat legacyDbDateFormatter=new SimpleDateFormat("yyyyMMdd");
					SimpleDateFormat legacyDbTimeFormatter=new SimpleDateFormat("HHmmss");
					
					String dateStartString=legacyDbDateFormatter.format(subPeriodStartReference.getTime());
					String timeStartString=legacyDbTimeFormatter.format(subPeriodStartReference.getTime());
					
					GregorianCalendar subPeriodEndReferenceForLegacyDbQueries=(GregorianCalendar) subPeriodEndReference.clone();
					subPeriodEndReferenceForLegacyDbQueries.set(GregorianCalendar.HOUR_OF_DAY, endOfDayHours);
					subPeriodEndReferenceForLegacyDbQueries.set(GregorianCalendar.MINUTE, endOfDayMinutes);
					String dateEndString=legacyDbDateFormatter.format(subPeriodEndReferenceForLegacyDbQueries.getTime());
					String timeEndString=legacyDbTimeFormatter.format(subPeriodEndReferenceForLegacyDbQueries.getTime());
					
					String storesQueryCondition=null;

					if (eventClassType.equals("structured")) {
						//TODO verificare se opportuno gestire eventChecked cos�
						eventChecked=true;
						
						//make condition string for STORE selection to be put in other queries
						if (eventClassStructuredAllStores) {
							storesQueryCondition="";
						}
						else {
							ResultSet mysqlDbStoresListResults = mysqlStoreListStatement.executeQuery("SELECT legacy_db_store_id FROM event_class_has_store WHERE event_class_id="+eventClassId);
							int i=0;
							storesQueryCondition="(";
							while (mysqlDbStoresListResults.next()) {
								if (i==0) {
									storesQueryCondition+="NGMVKNEG='"+mysqlDbStoresListResults.getString("legacy_db_store_id")+"'";
								}
								else {
									storesQueryCondition+=" OR NGMVKNEG='"+mysqlDbStoresListResults.getString("legacy_db_store_id")+"'";
								}
								
								i++;
							}		
							storesQueryCondition+=") AND";
						}
						

						
						switch (eventClassStructuredValueType) {
							case "money":
								query = "SELECT  SUM(NGMVVAVE) as ref_value FROM LIBEMME.NGMAMV WHERE "+storesQueryCondition+" (NGMVDTMV BETWEEN "+dateStartString+" AND "+dateEndString+" ) AND (NGMVORAVE BETWEEN "+timeStartString+" AND "+timeEndString+")";
								break;
							case "product_quantity":
								query="SELECT  SUM(NGMVQTVE) as ref_value FROM LIBEMME.NGMAMV,LIBEMME.NGMAAR,LIBEMME.NGTABA WHERE "+storesQueryCondition+" NGARKAR=NGMVKAR AND NGBAKEY='@CM' AND NGBACOD=NGARCTME AND NGMVQTVE <>0  AND (NGMVDTMV BETWEEN "+dateStartString+" AND "+dateEndString+" ) AND (NGMVORAVE BETWEEN "+timeStartString+" AND "+timeEndString+")";
								break;
							case "transaction_quantity":
								query="SELECT COUNT(DISTINCT NGMVRIFS) as ref_value FROM LIBEMME.NGMAMV WHERE "+storesQueryCondition+" NGMVQTVE<>0 AND (NGMVDTMV BETWEEN "+dateStartString+" AND "+dateEndString+" ) AND (NGMVORAVE BETWEEN "+timeStartString+" AND "+timeEndString+")";
								break;
						}
						System.out.println("eseguita: "+query);
						ResultSet refValueResultset=legacyStatement.executeQuery(query);
						
						if ((refValueResultset.next())&&(refValueResultset.getString("ref_value")!=null)) {
							int intCurrentValue=Math.round(Float.parseFloat(refValueResultset.getString("ref_value")));
							if ((eventClassStructuredComparisionType.equals("min_reached"))&&(intCurrentValue>eventClassIntReferenceValue)) {
								eventHappened=true;	
								eventClassTargetReached=true;
							}						
							else if ((eventClassStructuredComparisionType.equals("min_not_reached"))&&(intCurrentValue<eventClassIntReferenceValue)&&(subPeriodEndReference.equals(currentDate))&&(currentDateTime.after(endOfDay))) {
								eventHappened=true;
								eventClassTargetReached=true;
							}						
							else if ((eventClassStructuredComparisionType.equals("delta"))&&((intCurrentValue/eventClassIntReferenceValue)>eventClassDeltaCounter)) {
								eventHappened=true;
								eventClassDeltaCounter=intCurrentValue/eventClassIntReferenceValue;							
							}	
						}
					
					} else if (eventClassType.equals("compared_query")) {
						//TODO verificare se opportuno gestire eventChecked cos�
						eventChecked=true;
						String stringCurrentValue=null; 
						legacyDbResults = legacyStatement.executeQuery(eventClassQuery);
						System.out.println("eseguita: "+eventClassQuery);
						if ((legacyDbResults.next())&&(legacyDbResults.getString("result")!=null)) {
							stringCurrentValue = legacyDbResults.getString("result");	
							int intCurrentValue = 0;
							if (!eventClassComparedQueryComparisionType.equals("string_contained")) {
								intCurrentValue=Math.round(Float.parseFloat(stringCurrentValue));
							}
							//testato
							if ((eventClassComparedQueryComparisionType.equals("min_reached"))&&(intCurrentValue>eventClassIntReferenceValue)) {
								eventHappened=true;	
								eventClassTargetReached=true;
							}
							//testato
							else if ((eventClassComparedQueryComparisionType.equals("min_not_reached"))&&(intCurrentValue<eventClassIntReferenceValue)&&(subPeriodEndReference.equals(currentDate))&& (currentDateTime.after(endOfDay))) {
								eventHappened=true;
								eventClassTargetReached=true;
							}
							//testato
							else if ((eventClassComparedQueryComparisionType.equals("delta"))&&((intCurrentValue/eventClassIntReferenceValue)>eventClassDeltaCounter)) {
								eventHappened=true;
								eventClassDeltaCounter=intCurrentValue/eventClassIntReferenceValue;							
							}
							//testato
							else if ((eventClassComparedQueryComparisionType.equals("string_contained"))&& (stringCurrentValue.contains(eventClassComparedQueryStringReferenceValue))) {
								eventHappened=true;
								eventClassTargetReached=true;
							}

						}
						
					} else if (eventClassType.equals("raw_query")) {
						eventChecked = true;
						legacyDbResults = legacyStatement.executeQuery(eventClassQuery);
						System.out.println("eseguita: "+eventClassQuery);
						if (legacyDbResults.next()) {

							// Il risultato della query dev'essere messo su una
							// colonna chiamata "result" (quindi AS result)
							String currentQueryResult = legacyDbResults.getString("result");

							// current query results different than the last one
							//testata
							if ((!currentQueryResult.isEmpty()) && (!currentQueryResult.equals(eventClassLastQueryResult))) {
								eventHappened = true;
								eventClassTargetReached=true;
								query = "UPDATE event_class SET event_class_last_query_result=? WHERE event_class_id=?";
								mysqlPrepStatement = mysqlDbConnection.prepareStatement(query);
								mysqlPrepStatement.setString(1, currentQueryResult);
								mysqlPrepStatement.setInt(2, eventClassId);
								mysqlPrepStatement.executeUpdate();
							}
						}

					} else {
						// error
					}
					//testata
					if (eventChecked) {
						query = "UPDATE event_class SET event_class_last_check=? WHERE event_class_id=?";
						mysqlPrepStatement = mysqlDbConnection.prepareStatement(query);
						mysqlPrepStatement.setTimestamp(1, currentSqlDateTime);
						mysqlPrepStatement.setInt(2, eventClassId);
						mysqlPrepStatement.executeUpdate();
					}
					if (eventHappened) {
						System.out.println("Evento generato");
						//testata
						query = "UPDATE event_class SET event_class_target_reached=?, event_class_delta_counter=? WHERE event_class_id=?";
						mysqlPrepStatement = mysqlDbConnection.prepareStatement(query);
						mysqlPrepStatement.setBoolean(1, eventClassTargetReached);
						mysqlPrepStatement.setInt(2, eventClassDeltaCounter);
						mysqlPrepStatement.setInt(3, eventClassId);
						mysqlPrepStatement.executeUpdate();

						// Logs event happening to event_archive
						//testata
						query = "INSERT INTO event_archive (event_class_id,event_archive_datetime) VALUES (?,?)";
						mysqlPrepStatement = mysqlDbConnection.prepareStatement(query);
						mysqlPrepStatement.setInt(1, eventClassId);
						mysqlPrepStatement.setTimestamp(2, currentSqlDateTime);
						mysqlPrepStatement.executeUpdate();

						
						// Get messages related to the event for looking for
						// variables
						query = "SELECT * FROM message_class WHERE event_class_id=?";
						mysqlPrepStatement = mysqlDbConnection.prepareStatement(query);
						mysqlPrepStatement.setInt(1, eventClassId);
						ResultSet mysqlDbMessageClassResults = mysqlPrepStatement.executeQuery();
						String sommaRicavi=null;
						String unitaTotale=null;
						String unitaCategorie=null;
						String scontriniEmessi=null;
						String margineTotale=null;
						String codiciProdotti=null;
						
						//message class elements
						while (mysqlDbMessageClassResults.next()) {
							
							String messageClassContent = mysqlDbMessageClassResults.getString("message_class_content");
							int messageClassId = mysqlDbMessageClassResults.getInt("message_class_id");
							

							
							// search for variables
							String patternString = "(%.*?%)"; // regex for finding // variables inside string like %var%, tested.
							Pattern pattern = Pattern.compile(patternString);
							Matcher matcher = pattern.matcher(messageClassContent);
							while (matcher.find()) {
								//variables only appliable to structured events
								if (!eventClassType.equals("structured")){
									break;
								}
								String varName = matcher.group(1).replace("%", "");
								String varContent = null;
								ResultSet varContentResultSet=null;

								switch (varName) {
									case "somma-ricavi":
										if (sommaRicavi==null) {
											query = "SELECT  SUM(NGMVVAVE) as total_income FROM LIBEMME.NGMAMV WHERE "+storesQueryCondition+" (NGMVDTMV BETWEEN "+dateStartString+" AND "+dateEndString+" ) AND (NGMVORAVE BETWEEN "+timeStartString+" AND "+timeEndString+")";
											varContentResultSet=legacyStatement.executeQuery(query);
											varContentResultSet.next();
											sommaRicavi=varContentResultSet.getString("total_income");
										}
										messageClassContent=messageClassContent.replace("%"+varName+"%", sommaRicavi);																					
										break;
									case "unita-totale":
										if (unitaTotale==null) {
											query="SELECT  SUM(NGMVQTVE) as units_sold FROM LIBEMME.NGMAMV,LIBEMME.NGMAAR,LIBEMME.NGTABA WHERE "+storesQueryCondition+" NGARKAR=NGMVKAR AND NGBAKEY='@CM' AND NGBACOD=NGARCTME AND NGMVQTVE <>0  AND (NGMVDTMV BETWEEN "+dateStartString+" AND "+dateEndString+" ) AND (NGMVORAVE BETWEEN "+timeStartString+" AND "+timeEndString+")";
											varContentResultSet=legacyStatement.executeQuery(query);
											varContentResultSet.next();
											float floatTemp=Math.round(Float.parseFloat(varContentResultSet.getString("units_sold")));
											unitaTotale=Integer.toString(Math.round(floatTemp));
										}
										messageClassContent=messageClassContent.replace("%"+varName+"%", unitaTotale);																					
										break;
									case "unita-categorie":
										if (unitaCategorie==null) {
											query="SELECT  SUM(NGMVQTVE) as units_sold,NGBADESC as category FROM LIBEMME.NGMAMV,LIBEMME.NGMAAR,LIBEMME.NGTABA WHERE "+storesQueryCondition+" NGARKAR=NGMVKAR AND NGBAKEY='@CM' AND NGBACOD=NGARCTME AND NGMVQTVE <>0  AND (NGMVDTMV BETWEEN "+dateStartString+" AND "+dateEndString+" ) AND (NGMVORAVE BETWEEN "+timeStartString+" AND "+timeEndString+") GROUP BY NGARCTME,NGBADESC ORDER BY NGARCTME";
											varContentResultSet=legacyStatement.executeQuery(query);
											unitaCategorie="";
											boolean firstCycle=true;
											while (varContentResultSet.next()) {
												if (!firstCycle) {
													unitaCategorie+=", ";
												}
												unitaCategorie+=varContentResultSet.getString("category")+": "+Integer.toString(Math.round(Float.parseFloat(varContentResultSet.getString("units_sold"))));
												firstCycle=false;
											}
										}
										messageClassContent=messageClassContent.replace("%"+varName+"%", unitaCategorie);																					
										break;
									case "scontrini-emessi":
										if (scontriniEmessi==null) {
											query="SELECT COUNT(DISTINCT NGMVRIFS) as invoices_count FROM LIBEMME.NGMAMV WHERE "+storesQueryCondition+"  NGMVQTVE <>0  AND (NGMVDTMV BETWEEN "+dateStartString+" AND "+dateEndString+" ) AND (NGMVORAVE BETWEEN "+timeStartString+" AND "+timeEndString+")";
											varContentResultSet=legacyStatement.executeQuery(query);
											varContentResultSet.next();
											scontriniEmessi=varContentResultSet.getString("invoices_count");
										}
										messageClassContent=messageClassContent.replace("%"+varName+"%", scontriniEmessi);																					
										break;
									case "margine-totale":
										if (margineTotale==null) {
											query="SELECT SUM( ((ROUND(NGMVVAUN*100/(100+NGMVALIV),0))-NGDLCOST)*NGMVQTVE)as total_margin FROM LIBEMME.NGMAMV,LIBEMME.NGPZDL,LIBEMME.NGSENE WHERE "+storesQueryCondition+" NGMVKNEG=NGNEKNEG AND NGNEKLIS=NGDLKLIS AND NGMVKAR=NGDLKAR AND NGMVQTVE <>0 AND (NGMVDTMV BETWEEN "+dateStartString+" AND "+dateEndString+" ) AND (NGMVORAVE BETWEEN "+timeStartString+" AND "+timeEndString+")";
											varContentResultSet=legacyStatement.executeQuery(query);
											varContentResultSet.next();
											float floatTemp=Math.round(Float.parseFloat(varContentResultSet.getString("total_margin")));
											margineTotale=String.format("%.2f", floatTemp);
										}
										messageClassContent=messageClassContent.replace("%"+varName+"%", margineTotale);																					
										break;
									case "codici-prodotti":
										if (codiciProdotti==null) {
											query="SELECT DISTINCT NGMVKAR as product_code FROM LIBEMME.NGMAMV WHERE "+storesQueryCondition+"  NGMVQTVE <>0  AND (NGMVDTMV BETWEEN "+dateStartString+" AND "+dateEndString+" ) AND (NGMVORAVE BETWEEN "+timeStartString+" AND "+timeEndString+") ORDER BY NGMVKAR";
											varContentResultSet=legacyStatement.executeQuery(query);
											codiciProdotti="";
											boolean firstCycle=true;
											while (varContentResultSet.next()) {
												if (!firstCycle) {
													codiciProdotti+=", ";
												}
												codiciProdotti+=varContentResultSet.getString("product_code");
												firstCycle=false;
											}
										}
										messageClassContent=messageClassContent.replace("%"+varName+"%", codiciProdotti);																					
										break;
								}
								System.out.println(varName);
								System.out.println("eseguita: "+query);
							}
							System.out.println("Messaggio generato: "+messageClassContent);
							
							if (messageClassContent.length()>messageContentMaxSize) {
								messageClassContent="Dimensione messaggio superiore al consentito, cambiare testo del messaggio e/o le variabili in esso";
							}

							// get users subscribed to that message class
							//testata
							query = "SELECT * FROM user_has_message_class WHERE message_class_id=?";
							mysqlPrepStatement = mysqlDbConnection.prepareStatement(query);
							mysqlPrepStatement.setInt(1, messageClassId);
							ResultSet mysqlDbUserHasMessageClassResults = mysqlPrepStatement.executeQuery();
							// foreach user subscribed to that message_class
							while (mysqlDbUserHasMessageClassResults.next()) {
								int userId = mysqlDbUserHasMessageClassResults.getInt("user_id");
								int userHasMessageClassInterval = mysqlDbUserHasMessageClassResults.getInt("user_has_message_class_interval");
								Timestamp userHasMessageClassLastSend = mysqlDbUserHasMessageClassResults.getTimestamp("user_has_message_class_last_send");

								GregorianCalendar programmedDeliveryUtil;
								boolean updateLastSend = false;
								// delivery at the end of the day
								if (userHasMessageClassInterval == 998) {
									updateLastSend = true;
									programmedDeliveryUtil = (GregorianCalendar) currentDateTime.clone();
									programmedDeliveryUtil.set(GregorianCalendar.HOUR, endOfDayHours);
									programmedDeliveryUtil.set(GregorianCalendar.MINUTE, endOfDayMinutes);
								}
								//delivery after userHasMessageClassInterval minutes
								else {
									GregorianCalendar testValue = new GregorianCalendar();
									//testvalue=lastsend+interval
									testValue.setTimeInMillis(userHasMessageClassLastSend.getTime());
									testValue.add(GregorianCalendar.MINUTE, userHasMessageClassInterval);
									if (currentDateTime.after(testValue)) {
										updateLastSend = true;
										programmedDeliveryUtil = (GregorianCalendar) currentDateTime.clone();
									} else {
										programmedDeliveryUtil = testValue;
									}
								}
								Timestamp programmedDeliverySql = new Timestamp(programmedDeliveryUtil.getTimeInMillis());

								// Add message to message spool
								//testata
								query = "INSERT INTO message_spool (message_spool_content,message_spool_programmed_delivery,user_id,message_spool_push_sent) VALUES (?,?,?,false)";
								mysqlPrepStatement = mysqlDbConnection.prepareStatement(query);
								mysqlPrepStatement.setString(1, messageClassContent);
								mysqlPrepStatement.setTimestamp(2, programmedDeliverySql);
								mysqlPrepStatement.setInt(3, userId);
								mysqlPrepStatement.executeUpdate();

								if (updateLastSend) {
									// Update last_send field
									query = "UPDATE user_has_message_class SET user_has_message_class_last_send=? WHERE user_id=? AND message_class_id=?";
									mysqlPrepStatement = mysqlDbConnection.prepareStatement(query);
									mysqlPrepStatement.setTimestamp(1, programmedDeliverySql);
									mysqlPrepStatement.setInt(2, userId);
									mysqlPrepStatement.setInt(3, messageClassId);
									mysqlPrepStatement.executeUpdate();
								}

							}

						}

						

					}
				}//end event_class while
				
				// TODO Ciclo di pulizia di eventi scaduti o altro ?
				
				//Select messages to be notified
				//testato
				query = "SELECT * FROM message_spool INNER JOIN user on message_spool.user_id=user.user_id WHERE "
						+"((message_spool_programmed_delivery <= NOW() AND message_spool_push_sent=FALSE) "
						//if maybe the push got lost (because the token was changed, user off-line or other reason)
						+"OR (DATE_ADD(message_spool_programmed_delivery, INTERVAL 1 HOUR) <= NOW())) "
						//the device token has to be registered
						+"AND user_smartphone_token IS NOT NULL ";
				mysqlPrepStatement = mysqlDbConnection.prepareStatement(query);
				ResultSet mysqlDbMessageSpoolResults = mysqlPrepStatement.executeQuery();
				
				//Prepare tasks to be executed for sending push
				//testato
				final ExecutorService workers = Executors.newCachedThreadPool();
				Collection<Callable<Integer>> tasks=new ArrayList<Callable<Integer>>();
				while (mysqlDbMessageSpoolResults.next()) {
					int messageSpoolId=mysqlDbMessageSpoolResults.getInt("message_spool_id");
					String messageSpoolContent=mysqlDbMessageSpoolResults.getString("message_spool_content");
					int userId=mysqlDbMessageSpoolResults.getInt("message_spool_id");
					String userSmartphoneType=mysqlDbMessageSpoolResults.getString("user_smartphone_type");
					String userSmartphoneToken=mysqlDbMessageSpoolResults.getString("user_smartphone_token");
					if (userSmartphoneToken==null) {
						continue;
					}
					
					//TODO Ripristinare dopo dimostrazione
					//tasks.add(new PushService(apiKeyServer, userSmartphoneType, userSmartphoneToken, textMessage,messageSpoolId,apnCertificate,apnPassword));		
					
					//TODO Togliere dopo dimostrazione
					tempStatement.executeUpdate("INSERT INTO push_to_send SET push_token='"+userSmartphoneToken+"', message_spool_id='"+messageSpoolId+"'");					
					System.out.println("Push inviato");
				}
				
				
				
				//Executes the tasks, waiting that are completed
				//testato
				List<Future<Integer>> results = null;
				try {
					results = workers.invokeAll(tasks, 10, TimeUnit.SECONDS);
					
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				//TODO Ripristinare dopo dimostrazione
				//Updates the message_spool_push_sent where needed
				//testato
				/*
				query = "UPDATE message_spool INNER JOIN user on message_spool.user_id=user.user_id SET message_spool_push_sent=TRUE WHERE";
				int elementsToupdate=0;
				for (Future<Integer> resultElement:results) {
					int messageId = 0;
					try {
						messageId = resultElement.get().intValue();
					} catch (InterruptedException e) {
						e.printStackTrace();
					} catch (ExecutionException e) {
						e.printStackTrace();
					}
					catch (Exception e) {
						e.printStackTrace();
					}
					
					if (messageId!=0) {
						elementsToupdate++;
						if (elementsToupdate==1) {
							query+=" message_spool_id IN ("+messageId;
						}
						else {
							query+=","+messageId;
						}
					}
				}
				//query execution
				if (elementsToupdate>0) {
					query+=")";
					query+=" AND user_smartphone_token IS NOT NULL";
					mysqlPrepStatement = mysqlDbConnection.prepareStatement(query);
					mysqlPrepStatement.executeUpdate();
				}
				*/
				//Update the programmed_delivery for messages still not received (add 1 hour to it)
				//If i just change the mesage_push_sent it will continue to send push because programmed delivery is till more than hour earlier
				//TODO Valutare se migliorabile, se lo smartphone non viene mai acceso cos� invia messaggi push all'infinito ogni ora
				//Mettere contatore messaggi push (magari cambiando push sent a int), se arriva a 5 eliminare il messaggio?
				//o rinviare push (settarlo=false) quando lo smartphone rif� il login
				//testato
				
				
				try {
					Thread.sleep(3 * 1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				//TODO Rimettere dopo dimostrazione
				/*
				query = "UPDATE message_spool SET message_spool_programmed_delivery=DATE_ADD(message_spool_programmed_delivery, INTERVAL 1 HOUR) WHERE DATE_ADD(message_spool_programmed_delivery, INTERVAL 1 HOUR) <= NOW()";
				mysqlPrepStatement = mysqlDbConnection.prepareStatement(query);
				mysqlPrepStatement.executeUpdate();
				*/
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			try {
				Thread.sleep(secondsToWait * 1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}

	}

	
}