package dmoda.mobile;

import static org.junit.Assert.*;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.google.android.gcm.server.Message.Builder;

public class TestUnits {
	private static MySqlDb mysqlDb;
	private static LegacyDb legacyDb;
	private ResultSet mysqlDbEventClassResults;
	private ResultSet legacyDbResults;
	static Statement mysqlStatement;
	static Statement legacyStatement;
	private static String apiKeyServer = "AIzaSyD6jnbKJSpedTTH2Zvf2uDxBoy6fpc30L8";
	static Connection mysqlDbConnection;
	PreparedStatement mysqlPrepStatement;
	private static int endOfDayHours = 20;
	private static int endOfDayMinutes = 00;
	// seconds of sleeping for the thread
	private static int secondsToWait = 50;
	// message sent in push notification TODO change it
	static String textMessage = "ok";
	String query = null;

	@BeforeClass
	public static void openDbConnection() {
		mysqlDb = new MySqlDb("192.168.1.20", "dmoda_mobile", "dmoda_mobilepw", "dmoda_mobile");
	//	legacyDb = new LegacyDb("192.168.83.3", "2638", "DBA", "SQL","LIBBALDI");
		mysqlStatement = mysqlDb.connect();
		//legacyStatement = legacyDb.connect();
		mysqlDbConnection = mysqlDb.getConnection();
	}


	@Test
	public void sendPushSingleThread() {
		try {
			/*
			// Select messages to be notified
			query = "SELECT * FROM message_spool INNER JOIN user on message_spool.user_id=user.user_id WHERE " + "(message_spool_programmed_delivery <= NOW() AND message_spool_push_sent=FALSE) "
			// if maybe the push got lost (because the token was changed, user off-line or other reason)
					+ "OR (DATE_ADD(message_spool_programmed_delivery, INTERVAL 1 HOUR) <= NOW())";
			mysqlPrepStatement = mysqlDbConnection.prepareStatement(query);
			ResultSet mysqlDbMessageSpoolResults = mysqlPrepStatement.executeQuery();

			while (mysqlDbMessageSpoolResults.next()) {
				int messageSpoolId = mysqlDbMessageSpoolResults.getInt("message_spool_id");
				String messageSpoolContent = mysqlDbMessageSpoolResults.getString("message_spool_content");
				int userId = mysqlDbMessageSpoolResults.getInt("message_spool_id");
				String userSmartphoneType = mysqlDbMessageSpoolResults.getString("user_smartphone_type");
				String userSmartphoneToken = mysqlDbMessageSpoolResults.getString("user_smartphone_token");
				Builder gcmMessage = new Message.Builder();
			//	gcmMessage.addData("title", "My Game");
				gcmMessage.addData("message", "Nuovo messaggio");
				gcmMessage.collapseKey("dmodaMobileNotification");
				gcmMessage.delayWhileIdle(false);
				Sender sender = new Sender(apiKeyServer);
				try {
					Result result = sender.send(gcmMessage.build(), userSmartphoneToken, 5);
					if (result.getErrorCodeName() == null) {
						System.out.println (messageSpoolId);
					} else {
						fail();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}*/
			
			// Select messages to be notified
			query = "SELECT * FROM user WHERE user_id=4";
			mysqlPrepStatement = mysqlDbConnection.prepareStatement(query);
			ResultSet mysqlDbMessageSpoolResults = mysqlPrepStatement.executeQuery();

			mysqlDbMessageSpoolResults.next();

				String userSmartphoneToken = mysqlDbMessageSpoolResults.getString("user_smartphone_token");
				Builder gcmMessage = new Message.Builder();
			//	gcmMessage.addData("title", "My Game");
				gcmMessage.addData("message", "Nuovo messaggio");
				gcmMessage.collapseKey("dmodaMobileNotification");
				gcmMessage.delayWhileIdle(false);
				Sender sender = new Sender(apiKeyServer);
				try {
					Result result = sender.send(gcmMessage.build(), userSmartphoneToken, 5);
					if (result.getErrorCodeName() == null) {
						System.out.println ("Mandato");
					} else {
						fail();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		

	}

	//@Test
	public void sendPushMultiThread() {
		try {
			// Select messages to be notified
			query = "SELECT * FROM message_spool INNER JOIN user on message_spool.user_id=user.user_id WHERE " + "(message_spool_programmed_delivery <= NOW() AND message_spool_push_sent=FALSE) "
			// if maybe the push got lost (because the token was changed, user off-line or other reason)
					+ "OR (DATE_ADD(message_spool_programmed_delivery, INTERVAL 1 HOUR) <= NOW())";
			mysqlPrepStatement = mysqlDbConnection.prepareStatement(query);
			ResultSet mysqlDbMessageSpoolResults = mysqlPrepStatement.executeQuery();

			// Prepare tasks to be executed for sending push
			final ExecutorService workers = Executors.newCachedThreadPool();
			Collection<Callable<Integer>> tasks = new ArrayList<Callable<Integer>>();
			while (mysqlDbMessageSpoolResults.next()) {
				int messageSpoolId = mysqlDbMessageSpoolResults.getInt("message_spool_id");
				String messageSpoolContent = mysqlDbMessageSpoolResults.getString("message_spool_content");
				int userId = mysqlDbMessageSpoolResults.getInt("message_spool_id");
				String userSmartphoneType = mysqlDbMessageSpoolResults.getString("user_smartphone_type");
				String userSmartphoneToken = mysqlDbMessageSpoolResults.getString("user_smartphone_token");
				//tasks.add(new PushService(apiKeyServer, userSmartphoneType, userSmartphoneToken, textMessage, messageSpoolId));
			}

			// Executes the tasks, waiting that are completed
			List<Future<Integer>> results = null;
			try {
				results = workers.invokeAll(tasks, 10, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			int elementsToupdate=0;
			System.out.println("Dimensione multi-thread results: "+results.size());
			for (Future<Integer> resultElement:results) {
				int messageId = 0;
				try {
					messageId = resultElement.get().intValue();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
				if (messageId!=0) {
					elementsToupdate++;
					System.out.println (messageId);
					
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@AfterClass
	public static void closeDbConnection() {
		mysqlDb.close();
	}

}
