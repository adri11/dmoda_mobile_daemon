package dmoda.mobile;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class TestEditDb {
	private static MySqlDb mysqlDb;
	private static LegacyDb legacyDb;
	private ResultSet mysqlDbEventClassResults;
	private ResultSet legacyDbResults;
	static Statement mysqlStatement;
	static Statement legacyStatement;
	private static String apiKeyServer = "AIzaSyD6jnbKJSpedTTH2Zvf2uDxBoy6fpc30L8";
	static Connection mysqlDbConnection;
	PreparedStatement mysqlPrepStatement;
	private static int endOfDayHours = 20;
	private static int endOfDayMinutes = 00;
	// seconds of sleeping for the thread
	private static int secondsToWait = 50;
	// message sent in push notification TODO change it
	static String textMessage = "ok";
	String query = null;

	@BeforeClass
	public static void openDbConnection() {
		mysqlDb = new MySqlDb("localhost", "dmoda_mobile", "dmoda_mobilepw", "dmoda_mobile");
		legacyDb = new LegacyDb("192.168.83.3", "2638", "DBA", "SQL","LIBBALDI");
		mysqlStatement = mysqlDb.connect();
		legacyStatement = legacyDb.connect();
		mysqlDbConnection = mysqlDb.getConnection();
	}
	@Ignore
	public void insertMessageSpool() {
		for (int i = 0; i < 48; i++) {
			query = "INSERT INTO message_spool (message_spool_content,message_spool_programmed_delivery,user_id) VALUES ('prova',NOW(),1)";
			try {
				mysqlPrepStatement = mysqlDbConnection.prepareStatement(query);
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			try {
				mysqlPrepStatement.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Test
	public void setFalseMessageSpoolPushSent() {
		query = "UPDATE message_spool SET message_spool_push_sent=false";
		try {
			mysqlPrepStatement = mysqlDbConnection.prepareStatement(query);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			mysqlPrepStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@AfterClass
	public static void closeDbConnection() {
		mysqlDb.close();
	}
}
