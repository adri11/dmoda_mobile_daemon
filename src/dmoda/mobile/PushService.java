package dmoda.mobile;

import java.io.IOException;
import java.util.Stack;
import java.util.concurrent.Callable;

import org.json.JSONException;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.google.android.gcm.server.Message.Builder;

import javapns.Push;
import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;
import javapns.notification.PushNotificationPayload;

public class PushService implements Callable<Integer> {
	private String apiKeyServer,type,token,textMessage,apnCertificate,apnPassword;
	private int messageId;
	private int returnValue;
	
	public PushService(String apiKeyServer,String type, String token, String textMessage,int messageId, String apnCertificate, String apnPassword) {
		this.apiKeyServer=apiKeyServer;
		this.type=type;
		this.token=token;
		this.textMessage=textMessage;		
		this.messageId=messageId;
		this.apnCertificate=apnCertificate;
		this.apnPassword=apnPassword;
	}
	public Integer call () {
		if (type.equals("android")) {
			Builder gcmMessage = new Message.Builder();
			gcmMessage.addData("title", "D Moda Mobile");
			gcmMessage.addData("message", "Nuovo messaggio");
			gcmMessage.addData("msgcnt", "1");
			gcmMessage.collapseKey("dmodaMobilePush");
			//received the notification even if the device is idle
			gcmMessage.delayWhileIdle(false);
		//time to live:default=4 weeks
			Sender sender = new Sender(apiKeyServer);
			try {
				Result result = sender.send(gcmMessage.build(), token, 5);
				if (result.getErrorCodeName() == null) {
					returnValue=messageId;
				} else {
					returnValue=0;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		else if (type.equals("ios")) {
			PushNotificationPayload payload = PushNotificationPayload.complex();
			try {
				
				payload.addAlert("Nuovo messaggio");
				//time to live: 4 weeks
				payload.setExpiry(4 * 7 * 24 * 60 * 60);
				Push.payload(payload, apnCertificate, apnPassword, true, token);
				//TODO Cambiare
				returnValue=messageId;
			} catch (Exception e) {

				e.printStackTrace();
			}
			
		}

		else {
			
		}
		return new Integer(returnValue);
	}

	

}
