package dmoda.mobile;
import java.io.IOException;
import java.sql.*;

public class MySqlDb {
	private String host, user,password,databaseName,url;
	private Statement jdbcStatement = null;
	private Connection jdbcConnection=null;

	public MySqlDb (String host,String user,String password,String databaseName) {
		this.host=host;
		this.user=user;
		this.password=password;
		this.databaseName=databaseName;
		this.url = "jdbc:mysql://" + host + ":3306/" + databaseName;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	public Connection getConnection () {
		return jdbcConnection;
	}
	
	public Statement connect() {
			
			
			try {
				jdbcConnection = DriverManager.getConnection(url, user,password);
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			/*System.out.println("URL: " + url);
			System.out.println("Connection: " + con);*/
			
			try {
				jdbcStatement = jdbcConnection.createStatement();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return jdbcStatement;			
	}
	public void close () {
		try {
			this.jdbcStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			jdbcConnection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
