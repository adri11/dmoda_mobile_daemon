package dmoda.mobile;

import java.io.*;
import java.sql.*;
import java.util.Properties;

public class LegacyDb {
	private String host,port,url;
	private Statement jdbcStatement = null;
	private Properties props = new Properties();
	private Connection jdbcConnection=null;

	public LegacyDb (String host,String port,String user,String password,String dbName) {
		this.host=host;
		this.port=port;
		props.put("user", user);
		props.put("password", password);
		props.put("testOnBorrow",true);
		props.put("validationQuery","SELECT 1");
		
		this.url = "jdbc:sybase:Tds:" + host + ":"+port+"?ServiceName="+dbName;
		
		try {
			DriverManager.registerDriver((Driver) Class.forName(
					"com.sybase.jdbc4.jdbc.SybDriver").newInstance());
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
	public Connection getConnection () {
		return jdbcConnection;
	}
	
	public Statement connect() {
		try {
			jdbcConnection = DriverManager.getConnection
					   (this.url, props);
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
		try {
			jdbcStatement = jdbcConnection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return jdbcStatement;			
	}
	
	public void close () {
		try {
			this.jdbcStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			jdbcConnection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}

/*
CODICE FUNZIONANTE (da mettere nella classe Daemon)

LegacyDb legacyDb = new LegacyDb("192.168.83.3", "2638","QUERYUSER","QUERYUSER");
Statement legacyStatement = legacyDb.connect();
ResultSet rs=null;
try {
	rs=legacyStatement.executeQuery("SELECT * from libemme.cmtbsg");
} catch (SQLException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
try {
	rs.next();
} catch (SQLException e1) {
	// TODO Auto-generated catch block
	e1.printStackTrace();
}
try {
	System.out.println (rs.getString("CMSGKSG"));
} catch (SQLException e) {
	e.printStackTrace();
}
legacyDb.close();
*/